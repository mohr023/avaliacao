package com.viasoft.avaliacaotecnica.dao;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.viasoft.avaliacaotecnica.model.StatusNfeEntity;

@Repository
public interface StatusNfeRepository extends CrudRepository<StatusNfeEntity, Integer> {

	List<StatusNfeEntity> findAll(Specification<StatusNfeRepository> spec);
	
	
}
