package com.viasoft.avaliacaotecnica.rest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.viasoft.avaliacaotecnica.model.Status;
import com.viasoft.avaliacaotecnica.model.StatusNfeEntity;
import com.viasoft.avaliacaotecnica.model.StatusServicoVO;
import com.viasoft.avaliacaotecnica.model.StatusVO;

@CrossOrigin
@RestController
public class StatusNfeController {
	
	@PersistenceContext
	private EntityManager em;

	@RequestMapping("/atual")
	public List<StatusVO> consultaStatusAtual() {
		LocalDateTime maiorData = em.createQuery("SELECT dataHoraConsulta from StatusNfeEntity order by id desc", LocalDateTime.class).setMaxResults(1).getSingleResult();
		
		List<StatusNfeEntity> resultados = em.createQuery("from StatusNfeEntity where dataHoraConsulta = "
				+ "'" + maiorData.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceAll("T", " ") + "'" 
				, StatusNfeEntity.class).getResultList();
		
		return constroiResposta(resultados);
	}
	
	private List<StatusVO> constroiResposta(List<StatusNfeEntity> listaEntity) {
		Map<String, List<StatusServicoVO>> map = new HashMap<>();
		List<StatusVO> retorno = new ArrayList<>();
		listaEntity.stream()
			.forEach(e -> 
				map.computeIfAbsent(e.getAutorizador(), key -> new ArrayList<>())
					.add(new StatusServicoVO(e)));
		
		map.entrySet().forEach(entry -> retorno.add(new StatusVO(entry.getKey(), entry.getValue())));
		return retorno;
	}
	
	@RequestMapping("/atual/porAutorizador")
	public List<StatusVO> consultaStatusAtualPorAutorizador(@RequestParam(value="autorizador") String autorizador) {
		LocalDateTime maiorData = em.createQuery("SELECT dataHoraConsulta from StatusNfeEntity order by id desc", LocalDateTime.class).setMaxResults(1).getSingleResult();
		
		List<StatusNfeEntity> resultados = em.createQuery("from StatusNfeEntity where autorizador = '"
				+ autorizador
				+ "' and dataHoraConsulta = "
				+ "'" + maiorData.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceAll("T", " ") + "'" 
				, StatusNfeEntity.class).getResultList();
		
		return constroiResposta(resultados);
	}
	
	@RequestMapping("/atual/porData")
	public List<StatusVO> consultaStatusAtualPorAutorizador(@RequestParam(value="dataHoraInicial") String strDataHoraInicial,
			@RequestParam(value="dataHoraFinal") String strDataHoraFinal) {
		
		LocalDateTime dataHoraInicial = LocalDateTime.parse(strDataHoraInicial);
		LocalDateTime dataHoraFinal = LocalDateTime.parse(strDataHoraFinal);
		
		List<StatusNfeEntity> resultados = em.createQuery("from StatusNfeEntity where dataHoraConsulta between "
				+ "'" + dataHoraInicial.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceAll("T", " ") + "'"
				+ " and "
				+ "'" + dataHoraFinal.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).replaceAll("T", " ") + "'"
				, StatusNfeEntity.class).getResultList();
		
		return constroiResposta(resultados);
	}
	
	@RequestMapping("/autorizadorMaisIndisponivel")
	public JSONWrappedObject consultaStatusAtualPorAutorizador() {
		String autorizador = em.createQuery("SELECT autorizador from StatusNfeEntity where status != '" + Status.ONLINE.toString() + "'"
				+ " GROUP BY autorizador ORDER BY COUNT(id) DESC", String.class).setMaxResults(1).getSingleResult();
		
		return new JSONWrappedObject("{\"autorizador\":", "}", autorizador);
	}
	
}
