package com.viasoft.avaliacaotecnica.timer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.viasoft.avaliacaotecnica.dao.StatusNfeRepository;
import com.viasoft.avaliacaotecnica.model.Status;
import com.viasoft.avaliacaotecnica.model.StatusNfeEntity;

@Component
@EnableScheduling
public class NfeTimer {
	
	@Autowired
	private StatusNfeRepository repository;

	@Scheduled(initialDelay=10,fixedDelay=5*60*1000)
	public void consultaNfe() throws ParseException, ClientProtocolException, IOException {
		Document doc = Jsoup.connect("http://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx").get();
		LocalDateTime dataHora = LocalDateTime.now();

		Elements table = doc.select("#conteudoDinamico div table[class=tabelaListagemDados]");

		if(!table.isEmpty()) {
			processaTabela(table.get(0), dataHora);
			processaTabela(table.get(1), dataHora);
		}
	}

	private void processaTabela(Element table, LocalDateTime dataHora) {
		String textVersao = table.getElementsByTag("caption").get(0).getElementsByTag("span").get(0).text();
		Pattern pattern = Pattern.compile(".*WebServices Vers.*? ([\\d\\.]+)");
		Matcher matcher = pattern.matcher(textVersao);
		String versao = "";
		if(matcher.find()) {
			versao = matcher.group(1);
		}
		
		Elements tableBody = table.select("tbody");
		
		Iterator<Element> iterator = tableBody.get(0).getElementsByTag("tr").iterator();
		Map<Integer, String> cabecalhos = montaCabecalhos(iterator.next());
		
		while(iterator.hasNext()) {
			Element linha = iterator.next();
			Elements tds = linha.getElementsByTag("td");
			
			for (int i = 1; i < tds.size(); i++) {
				StatusNfeEntity statusNfe = new StatusNfeEntity();
				statusNfe.setAutorizador(tds.get(0).text());
				statusNfe.setVersao(versao);
				
				Element img = tds.get(i).getElementsByTag("img").first();
				statusNfe.setDataHoraConsulta(dataHora);
				statusNfe.setServico(cabecalhos.get(i));
				if(img == null) {
					statusNfe.setStatus(Status.NAO_INFORMADO);
				} else if(img.attr("src").contains("bola_verde")) {
					statusNfe.setStatus(Status.ONLINE);
				} else if(img.attr("src").contains("bola_vermelho")) {
					statusNfe.setStatus(Status.OFFLINE);
				} else if(img.attr("src").contains("bola_amarelo")) {
					statusNfe.setStatus(Status.OFFLINE_RECENTE);
				}
				
				
				repository.save(statusNfe);
			}
		}
	}

	private Map<Integer, String> montaCabecalhos(Element linha) {
		Map<Integer, String> cabecalhos = new HashMap<>();
		
		Elements tds = linha.getElementsByTag("th");
		for (int i = 0; i < tds.size(); i++) {
			Element td = tds.get(i);
			cabecalhos.put(i, td.text());
		}
		return cabecalhos;
	}
	
}
