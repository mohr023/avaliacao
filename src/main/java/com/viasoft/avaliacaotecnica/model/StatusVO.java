package com.viasoft.avaliacaotecnica.model;

import java.util.List;

public class StatusVO {

	private String autorizador;
	private List<StatusServicoVO> servicos;
	
	public StatusVO(String autorizador, List<StatusServicoVO> servicos) {
		this.autorizador = autorizador;
		this.servicos = servicos;
	}
	public String getAutorizador() {
		return autorizador;
	}
	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}
	public List<StatusServicoVO> getServicos() {
		return servicos;
	}
	public void setServicos(List<StatusServicoVO> servicos) {
		this.servicos = servicos;
	}
	
	
	
}
