package com.viasoft.avaliacaotecnica.model;

public class StatusServicoVO {

	private String servico;
	private String status;
	
	public StatusServicoVO(StatusNfeEntity e) {
		this.servico = e.getServico();
		this.status = e.getStatus().toString();
	}
	public String getServico() {
		return servico;
	}
	public void setServico(String servico) {
		this.servico = servico;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
