CREATE TABLE statusnfe (
	id SERIAL PRIMARY KEY NOT NULL,
	status varchar(30) NOT NULL,
	servico varchar(30) NOT NULL,
	data_hora_consulta timestamp without time zone,
	autorizador varchar(10) NOT NULL,
	versao varchar(10) NOT NULL
);